// console.log("Testing for d1 s24")

// ES6 is also known as ECMAScript 2015
// ECMAScript is the standard that is used to create implementation of the language, one which is JavaScript.

// where all browser vendor can implement (Appl, Google, Microsoft, Mozilla, etc.)

// New features to JavaScript

// [SECTION] Exponent Operator

console.log("ES6 Updates");
console.log("----------");
console.log("=> Exponent Operator");

// using Math Object Methods
const firstNum = Math.pow(8, 3);

console.log(firstNum);

// using the exponent operator
const secondNum = 8 ** 3;
console.log(secondNum);

// [SECTION] Template Literals
/*
	- Allows to write string without using the concatenation operator (+).
	- ${} is called placeholder when using template literals, and we can input variables or expression.

*/

console.log("------------");
console.log("=> Template Literals");

let name = "John";

// Pre-Template Literals
let message = "Hello " + name + "! Welcome to programming";

console.log(message);

// Strings using template literals
// Uses backticks(``) instead of ("") or ('')

message = `Hello ${name}! Welcome to programming!`

console.log("Message with Template literals"); 
console.log(message);

// [SECTION] Array Destructuring
// It allows us to name array elements with variableNames instead of using the index numbers.
/*
	- Syntax:
		let/const [variableName1, variableName2, variableName3] = arrayName;
*/


const fullName = ["Juan","Dela","Cruz"];



console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);
console.log("----------------");

const [firstName, secondName, lastName] = fullName;

console.log(`Hello ${firstName} ${secondName} ${lastName}! It is nice to meet you!`);

const person = {
	givenName: "Jane",
	middleName: "Dela",
	surName: "Cruz"
}


// Pre-object destructuring
console.log(person.givenName);
console.log(person.middleName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.middleName} ${person.surName}! It's good to see you again!`);

console.log("---------------");

const {givenName, middleName, surName} = person;


console.log(givenName);
console.log(middleName);
console.log(surName);

console.log(`Hello ${givenName} ${middleName} ${surName}! It's good to see you again!!`);

console.log("------------");
console.log("=> Arrow Functions");

// [SECTION] Arrow Functions
/*
	-compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions that will not be reused in any other portion of code
	- This will work with "function expression."

	example of funcExpression

	let funcExpression = function funcName() {
		console.log("Hello from the other side");
	}

	funcExpression();

*/


// laba();
// function laba(){}

/*
Syntax

let/const variableName = (parameter) => {
	//code to execute;
}

//invocation
variableName(argument);

*/

const hello = () => {
	console.log("Hello from the other side");
}

hello();

const students = ["John", "Jane", "Judyt"];

students.forEach(function(student) {
	console.log(`${student} is a student`)
})


students.forEach(student => {
	console.log(`${student} is a student`)
})

// forEach method with the use of arrow function

// anonymous function - a funciton that has no funciton name

// [SECTION] Implicit Return Statement
// There are instances when you can omit the "return" statement.
// Implicit return means - Returns the statement/value even withour the return keyword;
// const add = (x, y) =>{
// 	return x + y;
// }


const add = (x,y) => x + y;

let total = add(1,2);

console.log(total)

/*

funciton add(x,y) {
	return x+y;
}


let total = add(1,2);
console.log(total); // = 3
*/

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked.

// const greet = (name) => `Good morning, ${name}`;
// console.log(greet()); // no argument provided will result to undefined


const greet = (name = "User") => `Good morning ${name}`;

console.log(greet());
console.log(greet("John"));


// [SECTION] Class-Based Object Blueprint
// Another approach in creating an object with key and value;
// Allows creation/installation of object using classes as blueprints

// - The "constructor" is a special method of a class for creating/initializing objects for that calss.

/*
- Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
			// insert function outside our constructor
		}
*/


class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// the "new" operator creates/instantiates a new object with the given argument as value of its property.

const myCar = new Car();

console.log(myCar);

// Reassigning value of each property
// Mini-activity
// 1. Reassign using dot notation brand property with a value "Ford";
// 2. . Reassign using dot notation name property with a value "Ranger Raptor"
// 3. Reassign using dot notation 'year' property with a value 202
// 4. console.log() or display the object myCar


myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


